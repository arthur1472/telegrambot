#!/usr/bin/env python3.5
import sys
import os
import classes.class_url as class_url
import classes.class_telegram as class_telegram
import classes.class_functions as class_functions
import classes.class_sql as class_sql
import classes.class_message as class_message
import classes.class_plugins as class_plugins
import classes.class_settings as class_settings
import classes.class_users as class_users
import time
import json
import inspect

# Declaration of core variables
sql = class_sql.SQL()
sql.open_connection()

url_wrapper = class_url.UrlWrapper()
telegram = class_telegram.Telegram(url_wrapper, sql)
message_class = class_message.Message()
settings = class_settings.Settings(sql)
users_class = class_users.Users(sql)
plugins = class_plugins.Plugins()
plugins_dict = plugins.load_plugins()
plugins.startup_module(telegram, sql, url_wrapper, plugins_dict)

commands = plugins.command_dict(plugins_dict)

decoder = json.JSONDecoder()
offset = "0"
i = 0

# Infinite while loop to keep the bot running
while (True):
	# Check MySQL connection every 10 seconds
	if ((i % 10) == 0):
		sql.keep_alive()
		i = 0

	# Fetch updates from telegram
	updates = telegram.getUpdates(offset)

	# Decode message
	decoded = decoder.decode(updates)
	messages = decoded['result']

	# If there is 1 or more messages than process message
	if (messages.__len__() >= 1):
		offset = message_class.handle_message(messages, telegram, sql, url_wrapper, settings, users_class, commands, plugins, plugins_dict)

	i += 1
	# Make the bot sleep so it won't take up too much CPU
	time.sleep(1)



sql.close_connection()
