# README #

### What is this repository for? ###

* This is a telegram bot coded in python. It started as a project to learn python better, however it now is a project for fun.
* It has a plugin base which you can use to further expand the plugins/commands

### How do I set it up? ###

#### Configuration ####
* Go to telegram and speak to the BotFather (https://telegram.me/botfather). Create a new bot. Once you have your bot token edit the file * "classes/class_telegram.py" and replace TOKEN with your token.

* Import the sql file into the MySQL database.
* Edit the file "classes/class_sql.py" and edit the setting on the top to desire.


#### Dependencies ####
* Python (it was tested on 3.5, I can't guarantee it works on other versions)
* MySQL connector for Python (http://dev.mysql.com/downloads/connector/python/)
* MySQL database (it was tested on 5.5.5-10.0.21-MariaDB, I can't guarantee it works on other versions)
* Active internet connection 

#### Run the bot ####
##### Linux #####
* Give the test.py 755 rights.
* Run it with ./test.py

##### Windows #####
* Run test.py (it was tested on linux, I can't guarantee it works on windows)

### Who do I talk to? ###
* Repo owner