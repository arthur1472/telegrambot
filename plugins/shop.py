name = "shop"
command = "/shop"
description = "Shop where you can buy stuff"
usage = "/shop "
admin = False

def run(telegram, sql, url_wrapper, message, plugins):
	"""Runs the code linked to the command.

	Args:
		telegram (class_telegram instance): Instance of class_telegram.
		sql (class_sql instance): Instance of class_sql. 
		url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
		message (dictionary): Message returned by Telegram in a dictionary. 
		plugins (class_plugins instance): Instance of class_plugins. 
	"""
	message_content = message['message']
	chat_id = str(message_content['chat']['id'])
	text = message_content['text']
	
 