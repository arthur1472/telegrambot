name = "say"
command = "/say"
description = "Returns the text that is given"
usage = "/say <text>"
admin = False

def run(telegram, sql, url_wrapper, message, plugins):
	"""Runs the code linked to the command.

	Args:
		telegram (class_telegram instance): Instance of class_telegram.
		sql (class_sql instance): Instance of class_sql. 
		url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
		message (dictionary): Message returned by Telegram in a dictionary. 
		plugins (class_plugins instance): Instance of class_plugins. 
	"""
	message_content = message['message']
	chat_id = str(message_content['chat']['id'])
	text = message_content['text']
	if (text.__len__() <= 5):
		text = "Usage: " + usage
	else:
		text = text[text.split(" ")[0].__len__()+1:]
	
	if ("http://" in text):
		telegram.sendMessage(chat_id, text, disable_preview = "true")
	else:
		telegram.sendMessage(chat_id, text)
