name = "help"
command = "/help"
description = "Returns this list"
usage = "/help"
admin = False

def run(telegram, sql, url_wrapper, message, plugins):
	"""Runs the code linked to the command.

	Args:
		telegram (class_telegram instance): Instance of class_telegram.
		sql (class_sql instance): Instance of class_sql. 
		url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
		message (dictionary): Message returned by Telegram in a dictionary. 
		plugins (class_plugins instance): Instance of class_plugins. 
	"""
	message_content = message['message']
	chat_id = str(message_content['chat']['id'])

	commands = plugins.get_commands()
	help_commands = []

	for command in commands:
		help_commands.append(command)

	help_commands.sort()

	text = ""

	for command in help_commands:
		text = text + commands[command]['usage'] + " , " + commands[command]['description'] + "\n"


	telegram.sendMessage(chat_id, text) 