name = "bomb"
command = "/bomb"
description = "Bombs the thing you give it"
usage = "/bomb <thing to bomb>"
admin = False

def run(telegram, sql, url_wrapper, message, plugins):
	"""Runs the code linked to the command.

	Args:
		telegram (class_telegram instance): Instance of class_telegram.
		sql (class_sql instance): Instance of class_sql. 
		url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
		message (dictionary): Message returned by Telegram in a dictionary. 
		plugins (class_plugins instance): Instance of class_plugins. 
	"""
	message_content = message['message']
	chat_id = str(message_content['chat']['id'])
	text = message_content['text']
	
	if (text.__len__() <= command.__len__()+1):
		text = "Usage: " + usage
	else:
		text = text[text.split(" ")[0].__len__()+1:]
		text = "Klabaf weg "+text

		file_id = "AgADBAADrqcxG_1p6AeoV_cxAQE_qTmAaTAABEKzWHRCKAHTg3ABAAEC"
		
		telegram.sendPhoto(chat_id, file_id)
	
	telegram.sendMessage(chat_id, text)