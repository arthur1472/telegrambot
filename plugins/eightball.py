name = "eightball"
command = "/eightball"
description = "Gives you a yes or no to the question you ask"
usage = "/eightball <question>"
admin = False

def run(telegram, sql, url_wrapper, message, plugins):
	"""Runs the code linked to the command.

	Args:
		telegram (class_telegram instance): Instance of class_telegram.
		sql (class_sql instance): Instance of class_sql. 
		url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
		message (dictionary): Message returned by Telegram in a dictionary. 
		plugins (class_plugins instance): Instance of class_plugins. 
	"""
	import random

	message_content = message['message']
	chat_id = str(message_content['chat']['id'])

	QA = {	'yes': ('It is certain', 'It is decidedly so', 'Without a doubt',
				'Yes, definitely', 'You may rely on it', 'As I see it, yes',
				'Most likely', 'Outlook good', 'Yes', 'Signs point to yes'),
			'maybe': ('Reply hazy; try again', 'Ask again later',
				'Better not tell you now', 'Cannot predict now',
				'Concentrate and ask again'),
			'no': ('Don\'t count on it', 'My reply is no', 'My sources say no',
				'Outlook not so good', 'Very doubtful')}


	rand = random.randint(0,1)
	if (rand == 1):
		telegram.sendMessage(chat_id,random.choice(QA['yes']))
	else:
		telegram.sendMessage(chat_id,random.choice(QA['no']))