name = "weather"
command = "/weather"
description = "Returns the weather for the given city"
usage = "/weather <city>"
admin = False

def run(telegram, sql, url_wrapper, message, plugins):
	"""Runs the code linked to the command.

	Args:
		telegram (class_telegram instance): Instance of class_telegram.
		sql (class_sql instance): Instance of class_sql. 
		url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
		message (dictionary): Message returned by Telegram in a dictionary. 
		plugins (class_plugins instance): Instance of class_plugins. 
	"""
	import threading
	try:
		weather_thread = threading.Thread(target=weather, args=(telegram, sql, url_wrapper, message, plugins,))
		weather_thread.start()
	except:
		telegram.sendMessage(str(message['message']['chat']['id'], "Could not process command, try later"))
	

def weather(telegram, sql, url_wrapper, message, plugins):
	"""Send user weather info with the API of openweathermap.

	Args:
		telegram (class_telegram instance): Instance of class_telegram.
		sql (class_sql instance): Instance of class_sql. 
		url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
		message (dictionary): Message returned by Telegram in a dictionary. 
		plugins (class_plugins instance): Instance of class_plugins. 
	"""
	import json
	import classes.class_sql as class_sql
	sql = class_sql.SQL()
	sql.open_connection()

	message_content = message['message']
	chat_id = str(message_content['chat']['id'])
	text = message_content['text']
	message_id = message_content['message_id']

	markup = "None"
	if (text.__len__() <= 9):
		text = "Usage: " + usage
	else:
		city_name = text[text.split(" ")[0].__len__()+1:]
		country = "NL"
		query = sql.select("city_id, city_name", "cities", "city_name = %s LIMIT 1", (city_name, ))
		city = query.fetchone()

		if (query.rowcount == 1):
			city_id = city[0]
			city_name = city[1]
			api_key = "eba12962b579a849d7785391416bf9a4"
			output = url_wrapper.get("http://api.openweathermap.org/data/2.5/weather", "id=%s&APPID=%s" % (city_id, api_key))
			if (output != ""):
				output = output.decode('utf-8')

				decoder = json.JSONDecoder()
				decoded = decoder.decode(output)
				
				text = "City: " + city_name + "\n"
				text += "Current temperature: " + str(kelvintocelcius(decoded['main']['temp'])) + "\n"
				text += "Cloudiness: " + decoded['weather'][0]['description'] + "\n"
				text += "Minimal temperature: " + str(kelvintocelcius(decoded['main']['temp_min'])) + "\n"
				text += "Maximal temperature: " + str(kelvintocelcius(decoded['main']['temp_max'])) + "\n"
				text += "Humidity: " + str(decoded['main']['humidity']) + "%\n"
				text += "Wind: " + str(decoded['wind']['speed']) + "m/s\n"
			else:
				text = "Unable to complete call to API"
		else:
			text = "Could not find city"
			markup_query = sql.select("city_name", "cities", "city_name LIKE %s AND country = %s LIMIT 5", ("%"+city_name+"%", country, ))
			cities = markup_query.fetchall()
			cities_list = []
			for cityname in cities:
				cityname = "/weather " + cityname[0]
				new_list = []
				new_list.append(cityname)
				cities_list.append(new_list)

			markup = telegram.ReplyKeyboardMarkup(cities_list, one_time_keyboard = True, selective = True)
			markup_query.close()
		
		
		query.close()
	
	telegram.sendMessage(chat_id, text) if markup == "None" else telegram.sendMessage(chat_id, text, markup = markup, reply_id=message_id)

def kelvintocelcius(kelvin):
	"""Converts kelvin to celcius.

	Args:
		kelvin (int): Degrees in kelvin

	Returns:
		float: Degrees in celcius.
	"""
	standard = float('273.15')
	kelvin = float(kelvin)
	return round(kelvin - standard, 2)