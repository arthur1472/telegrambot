name = "stats"
command = "/stats"
description = "Shows your stats"
usage = "/stats"
admin = False

def run(telegram, sql, url_wrapper, message, plugins):
	"""Runs the code linked to the command.

	Args:
		telegram (class_telegram instance): Instance of class_telegram.
		sql (class_sql instance): Instance of class_sql. 
		url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
		message (dictionary): Message returned by Telegram in a dictionary. 
		plugins (class_plugins instance): Instance of class_plugins. 
	"""
	import locale
	locale.setlocale( locale.LC_ALL, '' )


	message_content = message['message']
	chat_id = str(message_content['chat']['id'])
	user_id = str(message_content['from']['id'])
	message_text = message_content['text']

	first_name = message_content['from']['first_name'] if "first_name" in message_content['from'] else "Unknown"

	text = ""

	messages_sent = 0
	money = 0
	participated = 0
	ignored = False


	cur = sql.select("*", "ignore_list", "user_id = %s", (user_id,))
	cur.fetchall()
	count = cur.rowcount
	if (count == 1):
		ignored = True
	cur.close()

	cur = sql.select("id", "messages", "user_id = %s AND chat_id = %s", (user_id, chat_id,))
	cur.fetchall()
	count = cur.rowcount
	if (count >= 1):
		messages_sent = count
	cur.close()

	cur = sql.select("money", "users", "user_id = %s", (user_id,))
	data = cur.fetchall()
	count = cur.rowcount
	if (count >= 1):
		money = locale.currency(float(data[0][0]), grouping=True)
	cur.close()

	cur = sql.select("*", "bankheists", "user_ids LIKE %s AND ended = %s", ("%" + user_id + "%",1,))
	cur.fetchall()
	count = cur.rowcount
	if (count >= 1):
		participated = count
	cur.close()


	text = """
	First name: %s
	User ID: %s
	Messages you sent in this group: %s
	Money: %s
	Times participated in heists: %s
	On ignore list: %s
	""" % (first_name, user_id, messages_sent, money, participated, ignored)

	if (text != ""):
		telegram.sendMessage(chat_id, text)