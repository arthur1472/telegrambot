name = "ignore"
command = "/ignore"
description = "Puts the user into an ignore list"
usage = "/ignore <user_id>"
admin = True

def run(telegram, sql, url_wrapper, message, plugins):
	"""Runs the code linked to the command.

	Args:
		telegram (class_telegram instance): Instance of class_telegram.
		sql (class_sql instance): Instance of class_sql. 
		url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
		message (dictionary): Message returned by Telegram in a dictionary. 
		plugins (class_plugins instance): Instance of class_plugins. 
	"""
	import time

	message_content = message['message']
	chat_id = str(message_content['chat']['id'])
	splitted = message_content['text'].split(" ")
	user_id = splitted[1]
	time = 0
	if (len(splitted) >= 3):
		if (int(splitted[2]) != 0):
			time = (time.time() + (float(splitted[2]) * float(3600)))

	reason = (splitted[3] if (len(splitted) >= 4) else "Unknown")

	cur = sql.select("COUNT(id)", "ignore_list", "user_id = %s", (user_id,))
	if cur.fetchone()[0] == "1":
		text = "Already ignoring user"
	else:
		sql.insert("ignore_list", "user_id = %s, till = %s, reason = %s", (user_id,time,reason,))
		text = "Ignoring user with id: " + user_id
	
	cur.close()
	telegram.sendMessage(chat_id, text) 
