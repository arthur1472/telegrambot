name = "bankheist"
command = "/bankheist"
description = "Plans a bankheist, needs 4 people to start one."
usage = "/bankheist"
admin = False

citycount = 0

def run(telegram, sql, url_wrapper, message, plugins):
	"""Runs the code linked to the command.

	Args:
		telegram (class_telegram instance): Instance of class_telegram.
		sql (class_sql instance): Instance of class_sql. 
		url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
		message (dictionary): Message returned by Telegram in a dictionary. 
		plugins (class_plugins instance): Instance of class_plugins. 
	"""
	global citycount
	import random
	import time
	message_content = message['message']
	chat_id = str(message_content['chat']['id'])
	user_id = str(message_content['from']['id'])
	message_text = message_content['text']
	parameters = []
	for parameter in message_text.split(" "):
		parameters.append(parameter)

	first_name = message_content['from']['first_name'] if "first_name" in message_content['from'] else "None"

	user_id_name = user_id + "|" + first_name

	text = ""
	if (len(parameters) >= 2):
		if (parameters[1] == "active"):
			users = ""
			user_id_like = "%" + user_id + "%"
			query = sql.select("*", "bankheists", "user_ids LIKE %s AND ended = %s AND chat_id = %s", (user_id_like,0,chat_id,))
			current_heist = query.fetchall()
			if (query.rowcount >= 1):
				current_heist = current_heist[0]
				heist_id = current_heist[0]
				heist_users = []
				heist_money = current_heist[2]
				heist_start_time = current_heist[3]
				heist_city = current_heist[4]
				heist_chat_id = current_heist[5]
				heist_ended = current_heist[6]

				for heist_user in current_heist[1].split(","):
					heist_users.append(heist_user)

				for user in heist_users:
					if (users == ""):
						users = user.split("|")[1]
					else:
						users = users + ", " + user.split("|")[1]

				participants_needed = 4 - len(heist_users)

				time_now = time.time()
				start_time = int(heist_start_time) + 3600
				minutes = (int(start_time - time_now) / 60)
				seconds = int((float("0."+str(minutes).split(".")[1]) * 60))
				time_remaining = str(minutes).split(".")[0] + ":" + str(seconds)

				if (int(heist_start_time) == 0):
					text = """
					Current heist info
					Status: Waiting
					City: %s
					Participants: %s
					Participants needed: %s
					""" % (heist_city,users,participants_needed)
				else:
					text = """
					Current heist info
					Status: Executing
					City: %s
					Time remaining: %s
					Participants: %s
					""" % (heist_city,time_remaining,users)
			else:
				text = "You are not participating in a heist."

	elif (int(chat_id) <= 0):
		user_id_like = "%" + user_id + "%"
		query = sql.select("*", "bankheists", "user_ids LIKE %s AND ended = %s AND chat_id = %s", (user_id_like,0,chat_id,))
		current_heist = query.fetchall()
		if (query.rowcount == 0):
			query = sql.select("*", "bankheists", "chat_id = %s AND start_time = %s AND ended != %s", (chat_id,0,1,))
			result = query.fetchall()
			if (query.rowcount != 1):
				minimum = int(str(random.randint(1,3))+"00000")
				maximum = int(str(random.randint(4,6))+"00000")
				money = random.randint(minimum,maximum)

				if (citycount == 0):
					citycount = sql.select("COUNT(id)", "cities").fetchone()[0]

				city_id = random.randint(0, citycount)
				city = sql.select("city_name, country", "cities", "id = %s LIMIT 1", (city_id,)).fetchone()
				city_name = city[0]
				city_country = city[1]

				city = city_name+" (%s)" % city_country

				#telegram.sendMessage(chat_id, text)
				sql.insert("bankheists", "user_ids = %s, money = %s, start_time = %s, city = %s, chat_id = %s", (user_id_name, money, 0, city, chat_id, ))

				text = """You are planning a bank heist on the bank of "%s", you need 3 more people to execute it.""" % city

			else:
				for heist in result:
					heist_id = heist[0]
					heist_users = []
					heist_money = heist[2]
					heist_start_time = heist[3]
					heist_city = heist[4]
					heist_chat_id = heist[5]
					heist_ended = heist[6]

					for heist_user in heist[1].split(","):
						heist_users.append(heist_user)
					if (len(heist_users) <= 3):
						if user_id not in heist_users:
							user_id_like = "%" + user_id + "%"
							query = sql.select("*", "bankheists", "user_ids LIKE %s AND ended = %s AND chat_id = %s", (user_id_like,0,chat_id,))
							query.fetchall()
							if (query.rowcount == 0):
								user_ids = heist[1]+","+user_id_name

								if (len(heist_users) == 3):
									start_time = int(time.time())
									users = ""
									heist_users.append(user_id_name)
									for user in heist_users:
										if (users == ""):
											users = user.split("|")[1]
										else:
											users = users + ", " + user.split("|")[1]

									sql.update("bankheists", "user_ids = %s, start_time = %s", "id = %s", (user_ids, start_time, heist_id,))
									telegram.sendMessage(chat_id, "You will participate in the heist on the bank of %s." % heist_city)
									telegram.sendMessage(chat_id, "The heist on %s with %s will be executed now. This will take 1 hour." % (heist_city,users))
								else:
									sql.update("bankheists", "user_ids = %s", "id = %s", (user_ids, heist_id,))
									text = "You will participate in the heist on the bank of  %s." % heist_city
							else:
								text = "You can only participate in 1 heist at the time."
						else:
							text = "You can only participate in 1 heist at the time."
					else:
						continue
		else:
			time_now = time.time()
			start_time = int(current_heist[0][3]) + 3600
			minutes = int((start_time - time_now) / 60)
			if (minutes >= 0):
				text = "You can only participate in 1 heist at the time. Your heist ends in %s minutes." % minutes
			else:
				text = "You can only participate in 1 heist at the time."
	else:
		text = "You can not start a bankheist on your own."

	if (text != ""):
		telegram.sendMessage(chat_id, text)
	

def start_up(telegram, sql, url_wrapper):
	"""Starts a thread with the function bankheist.

	Args:
		telegram (class_telegram instance): Instance of class_telegram.
		sql (class_sql instance): Instance of class_sql. 
		url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
	"""
	import threading
	try:
		bankheist_thread = threading.Thread(target=bankheist, args=(telegram, sql, url_wrapper,))
		bankheist_thread.start()
	except:
		print("Could not start bankheist, try later")

def bankheist(telegram, sql, url_wrapper):
	"""Infinite while loop to process bankheists.

	Args:
		telegram (class_telegram instance): Instance of class_telegram.
		sql (class_sql instance): Instance of class_sql. 
		url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
	"""
	import time
	import locale
	import classes.class_sql as class_sql

	sql = class_sql.SQL()
	sql.open_connection()

	locale.setlocale( locale.LC_ALL, '' )

	while(True):
		sql.keep_alive()

		query = sql.select("*", "bankheists", "start_time > %s AND ended = %s", (0,0,))
		current_heist = query.fetchall()

		if (query.rowcount >= 1):
			for heist in current_heist:
				heist_id = heist[0]
				heist_users = []
				heist_money = heist[2]
				heist_start_time = heist[3]
				heist_city = heist[4]
				heist_chat_id = heist[5]
				heist_ended = heist[6]
				users = ""

				for heist_user in heist[1].split(","):
					heist_users.append(heist_user)

				for user in heist_users:
					if (users == ""):
						users = user.split("|")[1]
					else:
						users = users + ", " + user.split("|")[1]

				time_now = time.time()
				start_time = int(heist_start_time) + 3600
				minutes = int(start_time - time_now)
				if (minutes <= 0):
					money_each = (int(heist_money) / 4)
					sql.update("bankheists", "ended = %s", "id = %s", (1, heist_id, ))
					for heist_user in heist_users:
						heist_user = heist_user.split("|")[0]

						query = sql.select("*", "users", "user_id = %s", (heist_user,))
						data = query.fetchall()
						if (query.rowcount != 0):
							sql.update("users", "money = money + %s", "user_id = %s", (money_each,heist_user,))
						else:
							sql.insert("users", "user_id = %s, money = %s", (heist_user, money_each,))

						query.close()
					
					money_each_converted = locale.currency(money_each, grouping=True)
					heist_money_converted = locale.currency(int(heist_money), grouping=True)
					#print("The heist on %s was successful. %s got away with a total of %s. You each earned %s." % (heist_city, users, heist_money_converted, money_each_converted))
					text = "The heist on %s was successful. %s got away with a total of %s. You each earned %s." % (heist_city, users, heist_money_converted, money_each_converted)

					file_id = "AgADBAADsacxG_1p6Ac5pv7bItS6EJlMjDAABKCD0fgPwHUFaL0AAgI"
					telegram.sendPhoto(heist_chat_id, file_id)
					telegram.sendMessage(heist_chat_id, text)
					
		time.sleep(10)