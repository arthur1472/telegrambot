class Message:
	"""Functions for processing messages"""

	spam_list = {}

	def __init__(self):
		pass

	def saveMessage(self, sql, update_id, message_id, chat_id, user_id, user_first_name, user_last_name, username, text, date, extra = None):
		insert_query = "update_id = %s, message_id = %s, chat_id = %s, user_id = %s, user_first_name = %s, user_last_name = %s, username = %s, text = %s, date = %s"
		insert_tuple = (update_id, message_id, chat_id, user_id, user_first_name, user_last_name, username, text, date)

		if (extra is not None):
			pass

		sql.insert("messages", insert_query, insert_tuple)

	def handle_message(self, messages, telegram, sql, url_wrapper, settings, users_class, commands, plugins, plugins_dict):
		import time
		offset = 0
		
		for message in messages:
			# Defining message variables
			message_content = message['message']

			update_id = message['update_id']
			message_id = message_content['message_id']
			chat_id = str(message_content['chat']['id'])
			user_id = message_content['from']['id']

			date = message_content['date']

			text = message_content['text'] if "text" in message_content else "None"
			first_name = message_content['from']['first_name'] if "first_name" in message_content['from'] else "None"
			last_name = message_content['from']['last_name'] if "last_name" in message_content['from'] else "None"
			username = message_content['from']['username'] if "username" in message_content['from'] else "Unkown"

			# Saving the message
			query = sql.select("*", "messages", "update_id = %s", (update_id,))
			query.fetchall()
			if (query.rowcount == 0):
				self.saveMessage(sql, update_id, message_id, chat_id, user_id, first_name, last_name, username, text, date)
				
			query.close()

			
			# Processing the message
			offset = str(update_id + 1)
			command = text
			if (" " in text):
				command = text.split(" ")[0]

			command = command.lower()

			if (len(command.split("@")) >= 2):
				command = command.split("@")[0]

			ignored = False

			cur = sql.select("*", "ignore_list", "user_id = %s", (user_id,))
			data = cur.fetchall()
			count = cur.rowcount
			
			if count != 0:
				till = float(data[0][2])
				if (time.time() >= till and till != 0):
					sql.delete("ignore_list", "user_id = %s", (user_id,))
				else:
					if (command == "/stats"):
						spam_list = self.spam_list
						if (user_id in spam_list):
							onetime = int(spam_list[user_id]["onetime"])
							if (onetime == 0):
								spam_list[user_id]["onetime"] = 1
								text = "To prevent spam you can not retrieve your stats anymore"
								telegram.sendMessage(chat_id, text)
							else:
								ignored = True
						else:
							spam_list[user_id] = {
								"messages": 1,
								"begintime": date,
								"onetime": 1
							}
							text = "To prevent spam you can not retrieve your stats anymore"
							telegram.sendMessage(chat_id, text)
						self.spam_list = spam_list
					else:
						ignored = True

			if ignored == False:
				cur.close()

				spam_apply = (True if (command.startswith("/")) else False)
			
				if (spam_apply):
					spam_list = self.spam_list
					spam_messages = int(settings.get_value("spam_messages").fetchone()[0])
					spam_time = int(settings.get_value("spam_time").fetchone()[0])

					if (user_id in spam_list):
						user = spam_list[user_id]
						user["messages"] += 1
						if ((date - user["begintime"]) <= spam_time):
							if (user["messages"] >= spam_messages):
								till = time.time() + 3600
								sql.insert("ignore_list", "user_id = %s, till = %s, reason = %s", (user_id, till, "Spam"))
								text = "You have been put on the ignore list for 1 hour due to spamming"
								telegram.sendMessage(chat_id, text)
								return offset
						else:
							user["begintime"] = date
							user["messages"] = 1
					else:
						spam_list[user_id] = {
							"messages": 1,
							"begintime": date,
							"onetime": 0
						}

					self.spam_list = spam_list

				admin_only = (True if (int(settings.get_value("admin_only").fetchone()[0]) == 1) else False)
				user_admin = users_class.is_admin(user_id)

				if (admin_only and not user_admin):
					break

				for command_name in commands:
					if (command == commands[command_name]['command']):
						if (commands[command_name]['admin'] and not user_admin):
							break

						plugins_dict[command_name].run(telegram, sql, url_wrapper, message, plugins)
						break
		return offset