class Telegram:
	"""docstring for Telegram"""

	bot_token = "TOKEN"
	bot_id = None
	base_url = "https://api.telegram.org/" + bot_token + "/"


	def __init__(self, UrlWrapper, SQL):
		self.url_wrapper = UrlWrapper
		self.sql = SQL
		

	def sendMessage(self, chat_id, text, **kwargs):
		part = "sendMessage"
		url_wrapper = self.url_wrapper

		parse_mode = kwargs.get("parse_mode", "")
		disable_preview = kwargs.get("disable_preview", False)
		reply_id = kwargs.get("reply_id", "")
		markup = kwargs.get("markup", "")

		data = {
			"chat_id": chat_id,
			"text": text,
			"parse_mode": parse_mode,
			"disable_web_page_preview": disable_preview,
			"reply_to_message_id": reply_id,
			"reply_markup": markup
		}
		
		response = url_wrapper.post(self.base_url+part, data)

	def sendPhoto(self, chat_id, photo, **kwargs):
		part = "sendPhoto"
		url_wrapper = self.url_wrapper

		caption = kwargs.get("caption", "")
		reply_id = kwargs.get("reply_id", "")
		markup = kwargs.get("markup", "")

		data = {
			"chat_id": chat_id,
			"photo": photo,
			"caption": caption,
			"reply_to_message_id": reply_id,
			"reply_markup": markup
		}

		response = url_wrapper.post(self.base_url+part, data)

	def ReplyKeyboardMarkup(self, keyboard, **kwargs):
		import json
		json_encoder = json.JSONEncoder()
		
		resize_keyboard = kwargs.get("resize_keyboard", False)
		one_time_keyboard = kwargs.get("one_time_keyboard", False)
		selective = kwargs.get("selective", False)

		keyboard_dict = {
			"keyboard": keyboard,
			"resize_keyboard": resize_keyboard,
			"one_time_keyboard": one_time_keyboard,
			"selective": selective
		}
		json_keyboard = json_encoder.encode(keyboard_dict)
		json_keyboard = json_keyboard.replace(', "', ',"')
		return json_keyboard
	
	def getUpdates(self, offset = ""):
		import json
		part = "getUpdates"
		url_wrapper = self.url_wrapper

		data = ""
		updates = url_wrapper.get(self.base_url+part, "offset="+offset)
		if (updates == ""):
			encoder = json.JSONEncoder()
			encoded = encoder.encode({"ok":"true","result":[]})
			data = encoded
		else:
			data = updates.decode("utf-8")
			#data = updates

		return data