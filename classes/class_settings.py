class Settings:
 	"""Self explaining functions for the settings"""

 	def __init__(self,SQL):
 		self.sql = SQL 		 

 	def get_settings(self):
 		query = self.sql.select("*", "settings")
 		return query

 	def get_value(self, setting):
 		query = self.sql.select("value", "settings", "name = %s", (setting,))
 		return query

 	def update_value(self, setting, new_value):
 		self.sql.update("settings", "value = %s", "name = %s", (new_value, setting,))