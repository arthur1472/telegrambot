class UrlWrapper:
	"""This is a class for url handling"""

	def __init__(self):
		from urllib.request import Request, urlopen
		from urllib import parse

		self.urlopen = urlopen
		self.Request = Request
		self.parse = parse
		self.last_url = ""
		self.last_data = ""

	def get(self, link, data = None):
		self.last_url = link
		try:
			if (data is None):
				opened_url = self.urlopen(link)
			else:
				opened_url = self.urlopen(link+"?"+data)

			read_data = opened_url.read()
			self.last_data = read_data

		except Exception as e:
			print(e)
			read_data = ""

		return read_data

	def post(self, link, data = None, headers = None):
		self.last_url = link

		data = self.parse.urlencode(data)
		data = data.encode('utf-8')

		if (headers is not None):
			req = self.Request(link, data, headers)
		else:
			req = self.Request(link, data)

		try:
			data = self.urlopen(req)
			read_data = data.read()
			self.last_data = read_data

		except Exception as e:
			print(e)
			read_data = ""

		return read_data

	def get_last_url(self):
		return self.last_url

	def get_last_data(self):
		return self.last_data