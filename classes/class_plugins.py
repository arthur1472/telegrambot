class Plugins:
	"""Function for everything related to plugins"""
	
	folder = "plugins"

	def __init__(self):
		import imp
		import os
		self.imp = imp
		self.os = os


	def get_plugins(self):
		"""Get all plugins from the plugins directory

		Returns:
			list: All plugins from the plugin directory
		"""
		os = self.os
		imp = self.imp
		folder = self.folder

		plugins = []
		possibleplugins = os.listdir(folder)
		for i in possibleplugins:
			i = i.split('.')[0]
			if ("__" in i or "data" in i):
				continue
			info = imp.find_module(i, [folder])
			plugins.append({"name": i, "info": info})
		return plugins

	def load_plugins(self):
		"""Load all plugins

		Returns:
			dictionary: All plugins loaded
		"""
		plugin_dict = {}
		for i in self.get_plugins():
		    print("Loading plugin " + i["name"])
		    plugin = self.load_module(i)
		    plugin_dict[i["name"]] = plugin

		return plugin_dict

	def load_module(self, plugin):
		return self.imp.load_module(plugin["name"], *plugin["info"])

	def command_dict(self, plugins):
		"""Puts all commands, descriptions, usages and admin values into a dictionary

		Args:
			plugins (dictionary): All plugins loaded

		Returns:
			dictionary: All commands, descriptions, usages and admin values
		"""
		commands = {}

		for plugin in plugins:
			commands[plugins[plugin].name] = {
				'command': plugins[plugin].command,
				'description': plugins[plugin].description,
				'usage': plugins[plugin].usage,
				'admin': plugins[plugin].admin
			}
		self.commands = commands
		return commands

	def get_commands(self):
		return self.commands

	def startup_module(self, telegram, sql, url_wrapper, plugins):
		"""Runs the startup function of the plugin if it has one.

		Args:
			telegram (class_telegram instance): Instance of class_telegram.
			sql (class_sql instance): Instance of class_sql. 
			url_wrapper (class_url_wrapper instance): Instance of class_url_wrapper. 
			plugins (class_plugins instance): Instance of class_plugins. 
		"""
		for plugin in plugins:
			try:
				plugins[plugin].start_up(telegram, sql, url_wrapper)
			except Exception as e:
				pass