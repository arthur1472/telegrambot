class Functions:
	"""Class with various functions to be reused"""
	
	def __init__(self):
		import hashlib
		self.hashlib = hashlib

	def md5(self, text):
		"""Converts the given text to MD5 hash.

		Args:
			text (string): Text that will be converted. 

		Returns:
			string: Text converted to MD5 hash
		"""
		m = self.hashlib.md5()
		text = text.encode(encoding="utf-8")
		m.update(text)
		md5hash = m.digest()

		return md5hash		