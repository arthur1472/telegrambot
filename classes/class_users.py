class Users:
	"""docstring for Users"""

	def __init__(self, sql):
		self.sql = sql

	def is_admin(self, user_id):
		sql = self.sql
		query = sql.select("admin", "users", "user_id = %s", (user_id,))
		is_admin = query.fetchone()
		if (query.rowcount == 1):
			is_admin = is_admin[0]
			if (is_admin == 1):
				return True
			else:
				return False
		else:
			return False