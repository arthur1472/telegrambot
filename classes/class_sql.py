class SQL:
	"""docstring for SQL"""

	username = ""
	password = ""
	database = ""
	host = "127.0.0.1"
	port = 3306
	
	def __init__(self):
		import mysql.connector
		self.mdb = mysql.connector

	def open_connection(self):
		#
		try:
			config = {
				'user': self.username,
				'password': self.password,
				'host': self.host,
				'database': self.database,
				'port': self.port,
				'autocommit': True,
				'unix_socket': """/var/lib/mysql/mysql.sock"""
			}

			self.con = self.mdb.MySQLConnection(**config)
		except Exception as e:
			print(e)
			exit()

	def close_connection(self):
		try:
			self.con.close()
		except Exception as e:
			print(e)
			exit()

	def get_connection(self):
		return self.con


	def keep_alive(self):
		if (not self.con.is_connected()):
			try:
				self.close_connection()
			except Exception as e:
				raise e
			
			self.open_connection()
			print("MySQL connection closed and opened")
		

	
	def select(self, column, table, where = None, where_tuple = None):
		cur = self.con.cursor()
		if (where is None):
			cur.execute("SELECT " + column + " FROM " + table)
		else:
			query = "SELECT " + column + " FROM " + table + " WHERE " + where
			cur.execute(query, where_tuple)

		return cur

	def update(self, table, column, where = None, values = ()):
		cur = self.con.cursor()
		if (where is None):
			cur.execute("UPDATE " + table + " SET " + column, values)
		else:
			cur.execute("UPDATE " + table + " SET " + column + " WHERE " + where, values)

		cur.close()

	def insert(self, table, values, values_tuple):
		cur = self.con.cursor()
		cur.execute("INSERT INTO " + table + " SET " + values, values_tuple)

		cur.close()

	def delete(self, table, where, where_tuple):
		cur = self.con.cursor()
		cur.execute("DELETE FROM " + table + " WHERE " + where, where_tuple)
		cur.close()